import unittest
import requests
import json

class TestReset(unittest.TestCase):

    SITE_URL = 'http://localhost:51065' # replace with your port id
    print("Testing for server: " + SITE_URL)
    RESET_URL = SITE_URL + '/reset/'
    MOVIES_URL = SITE_URL + '/movies/'

    def test_put_reset_index(self):
        m = {}
        r = requests.put(self.RESET_URL)
        test_a = 7
        test_b = 13
        test_c = 4
        
        # Get list of original movies
        r = requests.get(self.MOVIES_URL)
        resp = json.loads(r.content.decode('utf-8'))
        originals = resp['movies']
        origA = originals[test_a]
        origB = originals[test_b]
        origC = originals[test_c]

        # Change three movies with arbitrary ids
        m['title'] = 'A'
        m['genres'] = 'a'
        r = requests.put(self.MOVIES_URL + str(test_a), data = json.dumps(m))
        
        m['title'] = 'B'
        m['genres'] = 'b'
        r = requests.put(self.MOVIES_URL + str(test_a), data = json.dumps(m))

        m['title'] = 'C'
        m['genres'] = 'c'
        r = requests.put(self.MOVIES_URL + str(test_a), data = json.dumps(m))
    
        # Reset the index
        r = requests.put(self.RESET_URL)
        
        # Get the full list of movies after reset and check that the change ones are the same as the originals
        r = requests.get(self.MOVIES_URL)
        resp = json.loads(r.content.decode('utf-8'))
        test = resp['movies']
        testA = test[test_a]
        testB = test[test_b]
        testC = test[test_c]
        
        self.assertEqual(origA['title'], testA['title'])
        self.assertEqual(origA['genres'], testA['genres'])
        self.assertEqual(origB['title'], testB['title'])
        self.assertEqual(origB['genres'], testB['genres'])
        self.assertEqual(origC['title'], testC['title'])
        self.assertEqual(origC['genres'], testC['genres'])

    def test_put_reset_key(self):
        r = requests.put(self.RESET_URL) 
        test_id = 65
        original = {}
        
        # Get original movie data
        r = requests.get(self.MOVIES_URL + str(test_id))
        resp = json.loads(r.content.decode('utf-8'))
        original['title'] = resp['title']
        original['genres'] = resp['genres']
        
        # Change the data to something 
        new = {}
        new['title'] = 'ABC'
        new['genres'] = 'Test'
        r = requests.put(self.MOVIES_URL + str(test_id), data = json.dumps(new))

        # Reset the test id
        requests.put(self.RESET_URL + str(test_id))
        # Get and check to see if it matches original
        r = requests.get(self.MOVIES_URL + str(test_id))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['title'], original['title'])
        self.assertEqual(resp['genres'], original['genres'])

if __name__ == "__main__":
    unittest.main()

